%% BCA Exercise Session 5: Energy hub example
% Last modification: 21.4.2016
% Author: M.Hohmann
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
close all;
%% Load demand and pv data
load('EnergyHub.mat');

%% Definitions
% Optimization horizon
Horizon=8760; % hours
% Loads
heat_load=ehub_data.demand(:,2)';   % kW
power_load=ehub_data.demand(:,1)';  % kW
%% Grid connections
% Definitions
gas_price=0.063;    % CHF/kWh
power_price=0.096;  % CHF/kWh
% Grid variables
power=sdpvar(1,Horizon);
gas=sdpvar(1,Horizon);
% Grid limits (no selling of energy)
power_con_lim=power>=0;
gas_con_lim=gas>=0;

%% Combined heat and power plant
% Definitions
chp_capacity=50;    % capacity limit kW
chp_eff=0.3;        % gas to power efficiency
chp_capital=1500;   % capital costs CHF/kW
chp_hpr=1.73;       % heat to power ratio
% Capacity variables
chp_cap=sdpvar(1,1);
% Input and output variables
chp_in=sdpvar(1,Horizon);
chp_out=sdpvar(1,Horizon);
% Capacity limit constraints
chp_con_lim=[chp_cap>=0,chp_cap<=chp_capacity];
% Energy conversion constraints
chp_con_conv=[chp_out==chp_in*chp_eff,chp_in>=0,chp_out>=0,chp_out<=chp_cap];
% Combine constraints
chp_con=[chp_con_lim,chp_con_conv];
% Investment costs
chp_inv=chp_capital*chp_cap;

%% Natural-gas boiler
% Definitions
bo_capacity=100;    % capacity limit kW
bo_eff=0.7;         % gas to heat efficiency
bo_capital=200;     % capital costs CHF/kW
% Capacity variables
bo_cap=sdpvar(1,1);
% Input and output variables
bo_in=sdpvar(1,Horizon);
bo_out=sdpvar(1,Horizon);
% Capacity limit constraints
bo_con_lim=[bo_cap>=0,bo_cap<=bo_capacity];
% Energy conversion constraints
bo_con_conv=[bo_out==bo_in*bo_eff,bo_in>=0,bo_out>=0,bo_out<=bo_cap];
% Combine constraints
bo_con=[bo_con_lim,bo_con_conv];
% Investment costs
bo_inv=bo_capital*bo_cap;

%% Heat pump
% Definitions
hp_capacity=100;     % capacity limit kW
hp_eff=3.2;          % power to heat COP
hp_capital=1000;     % capital costs CHF/kW
% Capacity variables
hp_cap=sdpvar(1,1);
% Input and output variables
hp_in=sdpvar(1,Horizon);
hp_out=sdpvar(1,Horizon);
% Capacity limit constraints
hp_con_lim=[hp_cap>=0,hp_cap<=hp_capacity];
% Energy conversion constraints
hp_con_conv=[hp_out==hp_in*hp_eff,hp_in>=0,hp_out>=0,hp_out<=hp_cap];
% Combine constraints
hp_con=[hp_con_lim,hp_con_conv];
% Investment costs
hp_inv=hp_capital*hp_cap;

%% Photovoltaic panels
% Definitions
pv_capacity=50;      % capacity limit m^2
pv_eff=0.18;          % pv efficiency
pv_capital=437.5;     % capital costs CHF/m^2
% Solar irradiation in kW/m^2
pv_irradiation=1/1000*ehub_data.pv'; % in kW/m^2
% Capacity variables
pv_cap=sdpvar(1,1);
% Capacity limit constraints
pv_con_lim=[pv_cap>=0,pv_cap<=pv_capacity];
% Combine constraints
pv_con=pv_con_lim;
% PV output
pv_out=pv_eff*pv_irradiation(1:Horizon)*pv_cap;
% Investment costs
pv_inv=pv_cap*pv_capital;

%% Thermal storage
% Definitions
th_capacity=100;     % storage capacity limit kWh
th_decay=0.01;       % decay rate (storage losses) %/h/100
th_capital=100;      % capital costs CHF/kWh
th_loss=0.1;         % charging/discharging loss rate %/100
% Capacity variables
th_cap=sdpvar(1,1);
% Storage variables
th_in=sdpvar(1,Horizon);
th_out=sdpvar(1,Horizon);
th_store=sdpvar(1,Horizon+1);
% Capacity limit constraints
th_con_lim=[th_cap>=0,th_cap<=th_capacity,th_in>=0,th_out>=0,th_store>=0,th_store<=th_cap];
% Storage constraints
th_con_store=[th_store(2:end)==(1-th_decay)*th_store(1:end-1)+(1-th_loss)*th_in-1/(1-th_loss)*th_out,th_store(1)==0];
% Combine constraints
th_con=[th_con_lim,th_con_store];
% Investment costs
th_inv=th_capital*th_cap;

%% Battery
% Definitions
bat_capacity=100;     % storage capacity limit kWh
bat_decay=0.001;       % decay rate (storage losses) %/h/100
bat_capital=100;      % capital costs CHF/kWh
bat_loss=0.1;         % charging/discharging loss rate %/100
% Capacity variables
bat_cap=sdpvar(1,1);
% Storage variables
bat_in=sdpvar(1,Horizon);
bat_out=sdpvar(1,Horizon);
bat_store=sdpvar(1,Horizon+1);
% Capacity limit constraints
bat_con_lim=[bat_cap>=0,bat_cap<=bat_capacity,bat_in>=0,bat_out>=0,bat_store>=0,bat_store<=bat_cap];
% Storage constraints
bat_con_store=[bat_store(2:end)==(1-bat_decay)*bat_store(1:end-1)+(1-bat_loss)*bat_in-1/(1-bat_loss)*bat_out,bat_store(1)==0];
% Combine constraints
bat_con=[bat_con_lim,bat_con_store];
% Investment costs
bat_inv=bat_capital*bat_cap;

%% Balance equations
heat_con=th_out-th_in+chp_hpr*chp_out+hp_out+bo_out==heat_load(1:Horizon);
power_con=power+pv_out+chp_out-hp_in+bat_out-bat_in==power_load(1:Horizon);
gas_con=gas-bo_in-chp_in==0;

%% Collect all constraints
constraints=[power_con_lim,gas_con_lim,chp_con,bo_con,hp_con,pv_con,...
    th_con,bat_con,heat_con,power_con,gas_con];

%% Objective function
% Total costs: Investment costs + 20 years of energy costs
objective=chp_inv+bo_inv+hp_inv+pv_inv+th_inv+bat_inv+20*sum(gas_price*gas+power_price*power);

%% Start the optimization
% define MATLAB LP solver
ops=sdpsettings('solver','sedumi');
ops.sdpt3.maxit=200;
% optimize the design
optimize(constraints,objective,ops);

%% Output objective function value
value(objective)

%% Plot the results (access results with value(x), x being the optimized variable)
% Power
t=1:Horizon;
figure;
plot(t,power_load(1:Horizon),t,value(chp_out),t,value(-hp_in),t,value(pv_out),t,value(power),t,value(bat_out),t,value(-bat_in));
legend('Load','CHP','HP','PV','Grid','Battery out','Battery in');xlabel('Time [h]');ylabel('Output [kW]');
title('Power node');
% Heat
figure;
plot(t,heat_load(1:Horizon),t,value(chp_hpr*chp_out),t,value(hp_out),t,value(bo_out),t,value(th_out),t,value(-th_in));
legend('Load','CHP','HP','Boiler','Tank out','Tank in');xlabel('Time [h]');ylabel('Output [kW]');
title('Heat node');
% Gas
figure;
plot(t,value(chp_in),t,value(bo_in),t,value(gas));
legend('CHP','Boiler','Grid');xlabel('Time [h]');ylabel('Output [kW]');
title('Gas node');
%% End

